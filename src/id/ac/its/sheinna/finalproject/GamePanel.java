package id.ac.its.sheinna.finalproject;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class GamePanel implements Constants
{
	//	private final JPanel mousePanel;
	private JButton[] cardButtons = new JButton[110];
	private int[] values = new int[110];
	private int[][][] dp = new int[2][110][110]; //[siapa][curL][curR] -> buat ngestore nilai maksimal untuk setiap state
	private int[][][] opt = new int[2][110][110]; //buat ngestore langkah selanjutnya
	private int curL, curR, curPlayer;
	private JLabel player1,player2;
	private Integer numOfBox;
	private Integer tipe;
	private boolean isEnded;
	
	ScorePanel sp;
	GameTest gt;
	Random rand = new Random();
	public GamePanel(ScorePanel sp,Integer numOfBox,Integer tipe)
	{
		this.sp = sp;
		this.numOfBox = numOfBox;
		this.tipe = tipe;
		Icon gambar = new ImageIcon(getClass().getResource("player.png"));
		player1 = new JLabel(gambar);
		player2 = new JLabel(gambar);
		player1.setBounds(50, FRAME_HEIGHT - 200, LABEL_WIDTH, 150);
		player2.setBounds(FRAME_WIDTH - LABEL_WIDTH-50, FRAME_HEIGHT - 200, LABEL_WIDTH, 150);
		isEnded = false;
		fillArray(numOfBox);
		Integer i;
		curL = 1; curR = numOfBox;
		curPlayer = 0;
		Integer distToBorder = (ScreenWidth - ((BOX_TO_BOX_DISTANCE + BOX_WIDTH) * numOfBox - BOX_TO_BOX_DISTANCE)) / 2;
		Integer curX = distToBorder;
		for(i = 1; i <= numOfBox; i++)
		{
			Integer t1 = rand.nextInt(13) + 1; //random angka sesuai value kartu yang ada
			Icon card = new ImageIcon(getClass().getResource(t1 + ".png")); //manggil icon.png-nya
			cardButtons[i] = new JButton(card); //buttonnya dikasih icon sesuai value kartunya
			cardButtons[i].setActionCommand(t1.toString()); //set action command-nya tetap integer angkanya
			values[i] = t1;
			ButtonHandler handler = new ButtonHandler();
			SleepTestWithTimer handler2 = new SleepTestWithTimer();
			cardButtons[i].addActionListener(tipe == 1 ? handler : handler2);
			cardButtons[i].setBounds(curX,(ScreenHeight / 2) - (BOX_HEIGHT),BOX_WIDTH,BOX_HEIGHT);
			if(i != 1 && i != numOfBox) cardButtons[i].setEnabled(false);
			else cardButtons[i].setEnabled(true);
			curX += BOX_WIDTH + BOX_TO_BOX_DISTANCE;
		}
		getOptimalMoves(0,1,numOfBox);
		player1.setVisible(true);
		player2.setVisible(false);
	}
	
	public JLabel getLabelPlayer1() 
	{
		return player1;
	}
	
	public JLabel getLabelPlayer2() 
	{
		return player2;
	}

	public JButton get_cardButtons(Integer index)
	{
		return cardButtons[index];
	}

	public boolean get_isEnded()
	{
		return isEnded;
	}

	//gantinya memset :v
	private void fillArray(int numOfBox)
	{
		for (int i = 0 ; i < 2 ; i++)
			for (int j = 1 ; j <= numOfBox ; j++)
				for (int k = 1 ; k <= numOfBox ; k++)
				{
					dp[i][j][k] = -1;
					opt[i][j][k] = -1;
				}
	}

	//cari langkah paling optimal selanjutnya pake rekursi
	private int getOptimalMoves(int player,int left,int right)
	{
		if (left > right) return 0;
		if (dp[player][left][right] != -1) return dp[player][left][right];
		if (player == 0)
		{
			int ret = 1000;
			int takeLeft = values[left] + getOptimalMoves(1,left + 1,right);
			int takeRight = values[right] + getOptimalMoves(1,left,right - 1);
			if (takeLeft > takeRight)
			{
				dp[player][left][right] = takeLeft;
				opt[player][left][right] = 0;
			}
			else
			{
				dp[player][left][right] = takeRight;
				opt[player][left][right] = 1;
			}
		}
		else
		{
			int ret = 1000;
			int takeLeft = -values[left] + getOptimalMoves(0,left + 1,right);
			int takeRight = -values[right] + getOptimalMoves(0,left,right - 1);
			if (takeLeft < takeRight)
			{
				dp[player][left][right] = takeLeft;
				opt[player][left][right] = 0;
			}
			else
			{
				dp[player][left][right] = takeRight;
				opt[player][left][right] = 1;
			}
		}
		return dp[player][left][right];
	}

	//buat matiin & hilangin tombol
	private void turnOff(int index)
	{
		cardButtons[index].setEnabled(false);
		cardButtons[index].setVisible(false);
	}

	//buat diakhir game
	private void endGame()
	{
		isEnded = true;
		int score1 = Integer.parseInt(sp.getLabel1Text().split(" ")[2]);
		int score2 = Integer.parseInt(sp.getLabel2Text().split(" ")[2]);
		if(score1 == score2)
		{
			JOptionPane.showMessageDialog(null , String.format("TIE"));
		}
		else if(score1 > score2)
		{
			JOptionPane.showMessageDialog(null , String.format("Player 1 WIN"));
		}
		else
		{
			if (this.tipe == 1)
				JOptionPane.showMessageDialog(null, String.format("Player 2 WIN"));
			else
				JOptionPane.showMessageDialog(null, String.format("Computer WIN"));
		}
		player1.setVisible(false);
		player2.setVisible(false);
//		gt.frame.setVisible(false);
//		gt.frame.dispose();
//		new GameTest();
	}
	
	public int getValue()
	{
		int score1 = Integer.parseInt(sp.getLabel1Text().split(" ")[2]);
		return score1;
	}
	
	public int getValue2()
	{
		int score2 = Integer.parseInt(sp.getLabel2Text().split(" ")[2]);
		return score2;
	}
	
	private void turnOn(boolean left, boolean right)
	{
		player1.setVisible(left);
		player2.setVisible(right);
	}

	//buat cari curPlayer
	private void findCurPlayer()
	{
		if(curPlayer == 0)
		{
			sp.setLabel3Text("Player 1's turn");
			turnOn(true, false);
		}
		else 
		if(this.tipe == 1)
		{
			sp.setLabel3Text("Player 2's turn");
			turnOn(false, true);
		}
		else
		{
			sp.setLabel3Text("Computer's turn");
			turnOn(false, true);
		}
		if(curR < curL)
		{
			endGame();
		}
	}

	//buat cari ID kartu yang diambil
	private Integer findCardID(ActionEvent event)
	{
		Integer temp = 0;
		for(int i = curL; i <= curR; i++)
		{
			if(event.getSource() == cardButtons[i])
			{
				temp = i;
				break;
			}
		}
		return temp;
	}

	//buat nyalakan tombol sebelah tombol yang dimatikan
	private void turnOnNext(int index)
	{
		if(index == curR)
		{
			curR--;
			cardButtons[curR].setEnabled(true);
		}
		else
		{
			curL++;
			cardButtons[curL].setEnabled(true);
		}
	}

	//button handler buat vs player
	private class ButtonHandler implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			int temp1 = Integer.parseInt(sp.getLabel1Text().substring(10));
			int temp2 = Integer.parseInt(sp.getLabel2Text().substring(10));
			int delta = Integer.parseInt(event.getActionCommand());
			Integer id = 0;
			if(curPlayer == 0) temp1 += delta;
			else temp2 += delta;
			id = findCardID(event);
			if(curPlayer == 0)
			{
				Integer coba = (Integer)temp1;
				sp.setLabel1Text(coba.toString());
			}
			else
			{
				Integer coba = (Integer)temp2;
				sp.setLabel2Text(coba.toString());
			}
			turnOff(id);
			turnOnNext(id);
			curPlayer ^= 1;
			findCurPlayer();
		}
	}

	//button handler buat vs comp
	private class SleepTestWithTimer implements ActionListener {
	    private int delta, temp2;
	    private ButtonTextSetter button2SetO = new ButtonTextSetter(delta);
	    private Timer machineMoveTimer = new Timer(2000, button2SetO);
	    
	    public SleepTestWithTimer() {
	        machineMoveTimer.setRepeats(false);
	    }

	    public void actionPerformed(ActionEvent ae) {
	    	int temp1 = Integer.parseInt(sp.getLabel1Text().substring(10));
			int delta = Integer.parseInt(ae.getActionCommand());
			Integer id = 0;
			temp1 += delta;
			id = findCardID(ae);
			Integer currentScore = (Integer)temp1;
			sp.setLabel1Text(currentScore.toString());
			turnOff(id);
			turnOnNext(id);
			if(curR < curL)
			{
				endGame();
				return;
			}
	        machineMoveTimer.restart();
			curPlayer ^= 1;
			findCurPlayer();
	    }
	    
	}
	
	class ButtonTextSetter implements ActionListener {
	    private int delta;
	    
	    ButtonTextSetter(int delta) {
	        this.delta = delta;
	    }

	    public void actionPerformed(ActionEvent ae) {
	    	if (opt[1][curL][curR] == -1)
				getOptimalMoves(1,curL,curR);
			Integer take = opt[1][curL][curR];
			int temp2 = Integer.parseInt(sp.getLabel2Text().substring(10));
			if (take == 0)
			{
				//comp ambil kiri
				turnOff(curL);
				delta = Integer.parseInt(cardButtons[curL].getActionCommand());
				temp2 += delta;
				curL++;
				if (curR >= curL)
					cardButtons[curL].setEnabled(true);
			}
			else if (take == 1)
			{
				//comp ambil kanan
				turnOff(curR);
				delta = Integer.parseInt(cardButtons[curR].getActionCommand());
				temp2 += delta;
				curR--;
				if (curR >= curL)
					cardButtons[curR].setEnabled(true);
			}
			else {
				assert(false);
			}
			Integer coba = (Integer)temp2;
			sp.setLabel2Text(coba.toString());
			curPlayer ^= 1;
			findCurPlayer();
	    }
	}
	
}
