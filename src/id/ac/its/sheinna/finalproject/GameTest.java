package id.ac.its.sheinna.finalproject;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;

import javax.imageio.ImageIO;
import javax.swing.*;

public class GameTest implements Constants
{
	private JFrame mainFrame;
	private JButton newgameButton;
	private JFrame pauseFrame;
	private Integer tipe;
	private JFrame quitFrame;
	private JFrame historyFrame;
	private ArrayList scoreHistory = new ArrayList();
	private ArrayList scoreHistory2 = new ArrayList();
	
	ScorePanel panel2;
	GamePanel panel;
	BufferedImage image= null;
	

	GameTest(ArrayList scoreHistory, ArrayList scoreHistory2)
	{
		this.scoreHistory.addAll(scoreHistory);
		this.scoreHistory2.addAll(scoreHistory2);
		mainFrame=new JFrame("Main Menu");

		this.tipe = -1;
		try {
			image = ImageIO.read(new File("gambar_kartu/background.png"));

		}
		catch (IOException e){
			e.printStackTrace();
		}
		mainFrame.setContentPane(new backImage(image));		//Set Background

		Labels title = new Labels("G r e e d  i s  N o t  G o o d",ScreenWidth / 2 - 250,ScreenHeight / 2 - 50,
									500,50,40);
		title.getLabel().setFont(new Font("ProductSans",Font.BOLD,40));
		title.getLabel().setForeground(Color.white);
		
		//bikin button MultiPlayer
		buttonMouseListener player = new buttonMouseListener("Multiplayer",ScreenWidth / 2 + 10,
				ScreenHeight / 2 + 20,180,50,
				new Color(201,186,145,150));
		JButton playerButton = player.getButton();
		playerButtonListener playerHandler = new playerButtonListener(); //listener button VS Player
		playerButton.addActionListener(playerHandler);

		//bikin button SoloPlayer
		buttonMouseListener computer = new buttonMouseListener("Soloplayer",ScreenWidth / 2 - 190,
																	ScreenHeight / 2 + 20, 180,50,
																	new Color(201,186,145,150));
		JButton computerButton = computer.getButton();
		computerButtonListener computerHandler = new computerButtonListener(); //listener button VS Computer
		computerButton.addActionListener(computerHandler);

		//bikin button exit
		buttonMouseListener quit = new buttonMouseListener("Quit",ScreenWidth / 2 - 75, ScreenHeight / 2 + 200,
																150,50,
																new Color(224, 68, 54,200));
		JButton quitButton = quit.getButton();
		quitButtonListener quitListener = new quitButtonListener();
		quitButton.addActionListener(quitListener);

		buttonMouseListener history = new buttonMouseListener("History", FRAME_WIDTH / 2- 75,FRAME_HEIGHT / 2 + 80,
																150,50,
																new Color(201,186,145,150));
		JButton historyButton = history.getButton();
		historyButtonListener historyListener = new historyButtonListener();
		historyButton.addActionListener(historyListener);
		
		
		mainFrame.add(title.getLabel());
		mainFrame.add(playerButton);
		mainFrame.add(computerButton);
		mainFrame.add(quitButton);
		mainFrame.add(historyButton);
		defaultFrame();
	}

	private void defaultFrame() {
		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		mainFrame.setUndecorated(true);// Frame gabisa di edit
		mainFrame.pack();
		mainFrame.setLayout(null);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	//Fungsi buat bikin background di main
	private void makeFrame(String title,Integer tipe)
	{
		this.tipe=tipe;
		Random rand = new Random();
		Integer maxNumberOfCards = (ScreenWidth - 30) / (BOX_WIDTH + BOX_TO_BOX_DISTANCE);
		Integer numOfBox;
		numOfBox = rand.nextInt(maxNumberOfCards - 8 + 1) + 8;
		while ((numOfBox & 1) == 1) {
			numOfBox = rand.nextInt(maxNumberOfCards - 8 + 1) + 8;
		}

		mainFrame.dispose();
		mainFrame=new JFrame(title);

		try {
			image = ImageIO.read(new File("gambar_kartu/backdrop.png"));

		}
		catch (IOException e){
			e.printStackTrace();
		}
		mainFrame.setContentPane(new backImage(image));

		buttonMouseListener newGame = new buttonMouseListener("Pause",FRAME_WIDTH / 2 - 85 ,
																FRAME_HEIGHT - 70,170,50,
																new Color(201,186,145,150));
		JButton newgameButton = newGame.getButton();
		newgameButtonListener newgameHandler = new newgameButtonListener();
		newgameButton.addActionListener(newgameHandler);

		panel2 = new ScorePanel(numOfBox);
		panel = new GamePanel(panel2,numOfBox, tipe);
		for (int i = 1 ; i <= numOfBox ; i++)
			mainFrame.add(panel.get_cardButtons(i));
		mainFrame.add(panel.getLabelPlayer1());
		mainFrame.add(panel.getLabelPlayer2());
		mainFrame.add(panel2.getLabel1());
		mainFrame.add(panel2.getLabel2());
		mainFrame.add(panel2.getLabel3());
		mainFrame.add(newgameButton);
		defaultFrame();
	}

	private void makePauseFrame() {
		pauseFrame = new JFrame("Pause");

		Labels pauseLabel = new Labels("Game Paused !",10,30,200,50,20);


		//1. Continue
		buttonMouseListener kontinue = new buttonMouseListener("Continue",25,
				90, 170,50,
				new Color(3, 49, 252,150));
		JButton kontinueButton = kontinue.getButton();
		kontinueButtonListener kontinueHandler = new kontinueButtonListener(); //listener button VS Computer
		kontinueButton.addActionListener(kontinueHandler);

		//2. restart
		buttonMouseListener restart = new buttonMouseListener("Restart",25,
				150, 170,50,
				new Color(3, 49, 252,150));
		JButton restartButton = restart.getButton();
		if(tipe==1) {
			restartButton.addActionListener(new playerButtonListener());
		}
		else {
			restartButton.addActionListener(new computerButtonListener());
		}

		//3. Main menu
		buttonMouseListener mainmenu = new buttonMouseListener("Main Menu",25,
				210, 170,50,
				new Color(3, 49, 252,150));
		JButton mainmenuButton = mainmenu.getButton();
		mainmenuButton.addActionListener(new yesButtonListener());

		//4. Quit
		buttonMouseListener quit = new buttonMouseListener("Quit",25,
				270, 170,50,
				new Color(224, 68, 54,200));
		JButton quitButton = quit.getButton();
		quitButton.addActionListener(new quitButtonListener());

		try {
			image = ImageIO.read(new File("gambar_kartu/white.png"));

		}
		catch (IOException e){
			e.printStackTrace();
		}
		pauseFrame.setContentPane(new backImage(image));
		pauseFrame.setBounds(FRAME_WIDTH/2 - 110, FRAME_HEIGHT/2 - 175, 220, 350);

		pauseFrame.add(pauseLabel.getLabel());
		pauseFrame.add(kontinueButton);
		pauseFrame.add(restartButton);
		pauseFrame.add(mainmenuButton);
		pauseFrame.add(quitButton);

		pauseFrame.setUndecorated(true);
		pauseFrame.pack();
		pauseFrame.setSize(220,350);
		pauseFrame.setLayout(null);
		pauseFrame.setVisible(true);
		pauseFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void makeQuitFrame() {
		quitFrame = new JFrame();
		quitFrame.setBounds(FRAME_WIDTH/2 - 150, FRAME_HEIGHT/2 - 120, 300, 240);

		Labels labelFrame = new Labels("Do You Want To Stay ?",20,20,260,60,20);

		buttonMouseListener yes = new buttonMouseListener("Yes :)", 40, 100 ,100 , 100, new Color(28, 252, 3,150));
		JButton yesButton = yes.getButton();
		yesButton.addActionListener(new notQuitButtonListener());

		buttonMouseListener no = new buttonMouseListener("No :(",160,100,100,100, new Color(224, 68, 54,200));
		JButton noButton = no.getButton();
		noButton.addActionListener(new exitGame());

		try {
			image = ImageIO.read(new File("gambar_kartu/white.png"));

		}
		catch (IOException e){
			e.printStackTrace();
		}
		quitFrame.setContentPane(new backImage(image));

		quitFrame.add(labelFrame.getLabel());
		quitFrame.add(yesButton);
		quitFrame.add(noButton);

		quitFrame.setUndecorated(true);
		quitFrame.pack();

		quitFrame.setSize(300,240);
		quitFrame.setLayout(null);
		quitFrame.setVisible(true);
		quitFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void makeHistoryFrame() {
		historyFrame = new JFrame("History");
		historyFrame.setBounds(FRAME_WIDTH/2 - 125, FRAME_HEIGHT/2 - 150, 250, 300);
		Labels historyTitle = new Labels("Scratch The History", 10, 20, 230, 40, 20);
		
		Labels[] scoreTemp = new Labels[50];
		for(int i = 0; i < scoreHistory.size(); i++)
		{
			int size = (i * 25) + 70 ;
			scoreTemp[i] = new Labels(scoreHistory.get(i).toString() + " - " + scoreHistory2.get(i).toString(),10,size,230,40,20);
		}
		System.out.println("halo " + scoreHistory.size() +  " " + scoreHistory.get(0));
		buttonMouseListener back = new buttonMouseListener("Back", 40, 240, 170, 50, new Color(3, 49, 252,150));
		JButton backButton = back.getButton();
		backButtonListener backListener = new backButtonListener();
		backButton.addActionListener(backListener);
		
		try {
			image = ImageIO.read(new File("gambar_kartu/white.png"));

		}
		catch (IOException e){
			e.printStackTrace();
		}
		historyFrame.setContentPane(new backImage(image));
		
		for (int i = 0 ; i < scoreHistory.size() ; i++) {
			historyFrame.add(scoreTemp[i].getLabel());
		}
		historyFrame.add(historyTitle.getLabel());
		historyFrame.add(backButton);
		
		historyFrame.setUndecorated(true);
		historyFrame.pack();

		historyFrame.setSize(250,300);
		historyFrame.setLayout(null);
		historyFrame.setVisible(true);
		historyFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class playerButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			try {
				pauseFrame.dispose();
			}
			catch (Exception e){}
			finally {
				makeFrame("Multiplayer", 1);
			}
		}
	}
	private class computerButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			try {
				pauseFrame.dispose();
			}
			catch (Exception e){}
			finally {
				makeFrame("Soloplayer", 2);;
			}
		}

	}
	private class newgameButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			makePauseFrame();
		}


	}
	private class kontinueButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			pauseFrame.dispose();
		}

	}
	private class yesButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			pauseFrame.dispose();
			mainFrame.dispose();
			scoreHistory.add(panel.getValue());
			scoreHistory2.add(panel.getValue2());
			new GameTest(scoreHistory, scoreHistory2);
		}

	}
	private class notQuitButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			quitFrame.dispose();
			if (tipe != -1)
				makePauseFrame();
		}

	}
	private class exitGame implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}
	private class quitButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent event)
		{
			try {
				pauseFrame.dispose();
			}
			catch (Exception e) {}
			finally {
				makeQuitFrame();
			}
		}
	}

	private class noButtonListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			pauseFrame.dispose();
		}
	}
	
	private class historyButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			makeHistoryFrame();
		}
	}
	
	private class backButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			historyFrame.dispose();
		}
	}
	
	class backImage extends JComponent{

		BufferedImage image;

		public backImage(BufferedImage i) {
			this.image=i;
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		}
	}

	public static void main(String[] args)
	{
		ArrayList temp = new ArrayList();
		new GameTest(temp, temp);
	}

}
