package id.ac.its.sheinna.finalproject;

import javax.swing.*;
import java.awt.*;

public class Labels implements Constants {
    private JLabel label;

    Labels(String title,Integer x,Integer y,Integer width,Integer height,Integer fontSize) {
        label = new JLabel(title);
        label.setBounds(x,y,width,height);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(new Font("ProductSans",10,fontSize));
        label.setForeground(Color.black);
    }

    public JLabel getLabel() {
        return label;
    }
}
