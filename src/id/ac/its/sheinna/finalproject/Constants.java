package id.ac.its.sheinna.finalproject;

import java.awt.*;

//konstanta ukuran
public interface Constants {
    Dimension ScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Integer ScreenHeight = ScreenSize.height;
    Integer ScreenWidth = ScreenSize.width;
    Integer FRAME_HEIGHT = ScreenSize.height; //tinggi frame pas main
    Integer FRAME_WIDTH = ScreenSize.width;
    Integer LABEL_WIDTH = 100; //lebar label skor
    Integer LABEL_HEIGHT = 30; //tinggi label skor
    Integer BOX_WIDTH = 80;
    Integer BOX_HEIGHT = 120;
    Integer BOX_TO_BOX_DISTANCE = 15; //jarak antar kotak
    Color transparan = new Color(201,186,145,150);
    Font thisFont = new Font("ProductSans",10,25);
}
