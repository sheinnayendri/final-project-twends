package id.ac.its.sheinna.finalproject;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class ScorePanel implements Constants
{

	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	public ScorePanel(Integer numOfBox)
	{
		Icon p1 = new ImageIcon(getClass().getResource("player.png"));

		Color labelColor = new Color(240,162,17,150);
		Labels label1 = new Labels("Player 1: 0",50,FRAME_HEIGHT - 50,LABEL_WIDTH,LABEL_HEIGHT,15);
		label1.getLabel().setOpaque(true);
		label1.getLabel().setBackground(labelColor);
		this.label1 = label1.getLabel();

		Labels label2 = new Labels("Player 2: 0",FRAME_WIDTH - LABEL_WIDTH - 50,FRAME_HEIGHT - 50,LABEL_WIDTH, LABEL_HEIGHT,15);
		label2.getLabel().setOpaque(true);
		label2.getLabel().setBackground(labelColor);
		this.label2 = label2.getLabel();

		Labels label3 = new Labels("Player 1's turn",FRAME_WIDTH / 2 - (LABEL_WIDTH / 2) - 20, FRAME_HEIGHT - 150,
									LABEL_WIDTH + 40, LABEL_HEIGHT,20);
		label3.getLabel().setOpaque(true);
		label3.getLabel().setBackground(labelColor);
		this.label3 = label3.getLabel();
	}

	public JLabel getLabel1()
	{
		return label1;
	}

	public JLabel getLabel2()
	{
		return label2;
	}

	public JLabel getLabel3()
	{
		return label3;
	}

	public String getLabel1Text() {
		return label1.getText();
	}
	
	public String getLabel2Text() {
		return label2.getText();
	}
	
	public String getLabel3Text() {
		return label3.getText();
	}
	
	public void setLabel1Text(String s) {
		label1.setText("Player 1: " + s);
	}

	public void setLabel2Text(String s) {
		label2.setText("Player 2: " + s);
	}
	
	public void setLabel3Text(String s) {
		label3.setText(s);
	}
}
