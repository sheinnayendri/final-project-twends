package id.ac.its.sheinna.finalproject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class buttonMouseListener implements Constants {
    private JButton button;
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private Color color;

    buttonMouseListener(String title,Integer x,Integer y,Integer width,Integer height,Color color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        button = new JButton(title);
        listener Listener = new listener();
        button.setFont(thisFont);
        button.setForeground(Color.WHITE);
        button.setBackground(transparan);
        button.addMouseListener(Listener);
        button.setBounds(x,y,width, height); //buat ngatur posisi dan ukuran button VS Player
        button.setBorderPainted(false);
        button.setHorizontalAlignment(SwingConstants.CENTER);
    }

    public JButton getButton() {
        return button;
    }

    private class listener implements MouseListener {
        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            button.setBounds(x + 1, y, width, height);
            button.setBounds(x,y,width,height);
            button.setBackground(transparan);
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            button.setBackground(color);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent arg0) {
        }

        @Override
        public void mousePressed(MouseEvent arg0) {
        }
    }
}
